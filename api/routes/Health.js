
const express = require('express');
const request = require('request');
const router = express.Router();
router.get('/', (req, res, next) => {
    res.status(200).send({ 'status': 'Connection successful' });   
});
router.get('/Error', (req, res, next) => {
    process.exit(1)
    res.status(200).send({ 'status': 'Connection unsuccessful' });   
});
module.exports = router;